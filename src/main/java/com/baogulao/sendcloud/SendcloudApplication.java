package com.baogulao.sendcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SendcloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(SendcloudApplication.class, args);
    }

}
